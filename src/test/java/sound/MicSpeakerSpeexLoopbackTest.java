package sound;

import jni.JNI;

/**
 * @author Yann Massard
 */
public class MicSpeakerSpeexLoopbackTest implements Recorder.RecorderListener {


    private final Recorder recorder;
    private final Player player;
    private int LINE = 0;

    public static void main(String[] args) {
        MicSpeakerSpeexLoopbackTest test = new MicSpeakerSpeexLoopbackTest();
    }

    public MicSpeakerSpeexLoopbackTest() {
        prepareSpeexEncoderAndDecoder();
        recorder = new Recorder(1);
        player = new Player(1);
        recorder.setRecorderListener(this);
    }

    private void prepareSpeexEncoderAndDecoder() {
        JNI.initSpeexEncoder(LINE, JNI.SPEEX_MODEID_WB);
        JNI.encoderCtl(LINE, 12, 0); //12: VBR
        JNI.encoderCtl(LINE, 4, 5); //4: Quality
        JNI.encoderCtl(LINE, 16, 4); //16: Complexity
        JNI.encoderCtl(LINE, 30, 1); //30: VAD

        JNI.initSpeexDecoder(LINE, JNI.SPEEX_MODEID_WB);
    }

    public void onSoundData(short[] soundData) {

        byte[] speex = new byte[1024];

        int numBytes = JNI.encodeSpeex(LINE, soundData, speex);
        System.out.println("Speex bytes: " + numBytes);
        short[] decodedData = new short[320];
        JNI.decodeSpeex(LINE, speex, numBytes, decodedData);

        player.addSound(decodedData);
    }
}
