package sound;

import jni.JNI;
import sound.util.PcmUtil;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class TestUseAllLinesSeveralTimes {

    public static void main(String[] args) throws Exception {

        int line = 0;
        byte[] pcmBytesIn = new byte[640];
        short[] pcmShortsIn = new short[320];
        short[] pcmShortsOut = new short[320];
        byte[] pcmBytesOut = new byte[640];
        byte[] speex = new byte[200];

        for (int k = 0; k < 10000; k++) {
            line = k % 1000;
            FileInputStream in = new FileInputStream("tenSeconds.pcm");
            FileOutputStream out = new FileOutputStream("outTest/tenSecondsEncodedAndReDecoded" + k + ".pcm");

            JNI.initSpeexEncoder(line, JNI.SPEEX_MODEID_WB);
            JNI.encoderCtl(line, 12, 0); // 12: VBR
            JNI.encoderCtl(line, 4, 5); // 4: Quality
            JNI.encoderCtl(line, 16, 2); // 16: Complexity
            JNI.encoderCtl(line, 30, 1); // 30: VAD

            JNI.initSpeexDecoder(line, JNI.SPEEX_MODEID_WB);

            while (in.read(pcmBytesIn) != -1) {
                PcmUtil.pcmByteArrayToPcmShortArray(pcmBytesIn, pcmShortsIn);
                int numBytes = JNI.encodeSpeex(line, pcmShortsIn, speex);
                JNI.decodeSpeex(line, speex, numBytes, pcmShortsOut);
                PcmUtil.pcmShortArrayToPcmByteArray(pcmShortsOut, pcmBytesOut);

                out.write(pcmBytesOut);
            }

            System.out.println(line);
            JNI.destroySpeexEncoder(line);
            JNI.destroySpeexDecoder(line);

            in.close();
            out.close();

        }
        System.out.println("Finished.");

    }

}
