package sound;

import jni.JNI;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;


public class Test_Sound implements Recorder.RecorderListener {
    public static int STATE_TEST = 1;
    public static int STATE_EVAL_TEST = 2;
    public static int STATE_CREATE_PB_FILE = 3;

    public static int MIC_PORT = 1;
    public static int ECHO_PRODUCE_PORT = 1;
    public static int HEAD_SET_PORT = 3;
    public static String soundFile = "test-sound.raw";
    public static String rec = "rec.raw";


    public int state = STATE_EVAL_TEST;
    private Recorder recorder;
    private Player play;
    private Player player;

    private BufferedOutputStream bos;
    private BufferedInputStream bis;

    private static final int LINE = 0;


    public Test_Sound() {
        JNI.initSpeexEncoder(LINE, JNI.SPEEX_MODEID_WB);
        JNI.encoderCtl(LINE, 12, 0); //12: VBR
        JNI.encoderCtl(LINE, 4, 5); //4: Quality
        JNI.encoderCtl(LINE, 16, 4); //16: Complexity
        JNI.encoderCtl(LINE, 30, 1); //30: VAD

        JNI.preprocessStateInit(LINE, 320, 16000);
        JNI.preprocessCtl(LINE, 0, 1); //0: Denoise
        JNI.preprocessCtl(LINE, 2, 1); //2: AGC
        JNI.preprocessCtl(LINE, 4, 1); //4: VAD

        JNI.initSpeexDecoder(LINE, JNI.SPEEX_MODEID_WB);

        JNI.echoStateInit(LINE, 320, 3000);  //####################

        JNI.bindEchoCancellationToPreprocessor(LINE);
//		JNI.preprocessCtl(LINE, 20, -20);
//		JNI.preprocessCtl(LINE, 22, -75);

        play = new Player(HEAD_SET_PORT);
        player = new Player(ECHO_PRODUCE_PORT);
        recorder = new Recorder(MIC_PORT);
        recorder.setRecorderListener(this);
        try {
            if (state == STATE_TEST) {
                bos = new BufferedOutputStream(new FileOutputStream(new File(rec)));
                bis = new BufferedInputStream(new FileInputStream(new File(soundFile)));
            } else if (state == STATE_EVAL_TEST) {
                bis = new BufferedInputStream(new FileInputStream(new File(rec)));
            } else if (state == STATE_CREATE_PB_FILE) {
                bos = new BufferedOutputStream(new FileOutputStream(new File(soundFile)));

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void onSoundData(short[] s) {
        byte[] speex = new byte[200];

//		speex_echo_cancellation(st, ref_buf, echo_buf, e_buf);
//	      speex_preprocess_run(den, e_buf);

        //encode..
//		JNI.speex_echo_capture(LINE, s, s);
        short[] s3 = null;
        if (state == STATE_TEST) {
            s3 = readShorts();
            JNI.speex_echo_cancellation(LINE, s, s3, s);
            JNI.preprocessRun(LINE, s);
        } else if (state == STATE_EVAL_TEST) {
            s3 = readShorts();
        }
        int numBytes = JNI.encodeSpeex(LINE, s, speex);
        System.out.println("Speex bytes: " + numBytes);


        short[] s2 = new short[320];
        //decode..
        JNI.decodeSpeex(LINE, speex, numBytes, s2);

//		System.out.println("New data:" + s.length);

        int value = JNI.preprocessCtl(LINE, 21, 0);
        System.out.println("value: " + value);
        value = JNI.preprocessCtl(LINE, 23, 0);
        System.out.println("value: " + value);

        //encode..
        //decode..
        if (state == STATE_TEST) {
            player.addSound(s3);
//			JNI.speex_echo_playback(LINE, s3);
            writeShorts(s2);
        } else if (state == STATE_EVAL_TEST) {

            player.addSound(s3);
        } else if (state == STATE_CREATE_PB_FILE) {
            writeShorts(s);
        }

        System.out.println("New data:" + s.length);
        play.addSound(s2);
    }

    public void writeShorts(short[] s) {
        try {
            byte[] b = new byte[640];
            for (int j = 0; j < s.length; j++) {
                b[j * 2] = (byte) (s[j] & 0xff);
                b[j * 2 + 1] = (byte) (s[j] >>> 8);
            }
            bos.write(b);
            bos.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public short[] readShorts() {
        try {
            byte[] b = new byte[640];
            int r = bis.read(b, 0, b.length);
            if (r < 0) {
                System.out.println("ERROR EOF:" + r);
                return null;
            }
            if (r < 640) System.out.println("ERROR READ:" + r);
            short[] s = new short[320];
            for (int j = 0; j < s.length; j++) {
                s[j] = (short) ((b[2 * j + 1] << 8) | (b[2 * j] & 0xff));
            }
            return s;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }



    public static void main(String[] args) {
        new Test_Sound();
    }




}