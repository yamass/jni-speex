package sound;

import sound.util.PcmUtil;

import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;

/**
 * @author Yann Massard
 */
public class RecordPcmToFile {

    public static void main(String[] args) throws FileNotFoundException {
        final Recorder recorder = new Recorder(1);

        final FileOutputStream fileOutputStream = new FileOutputStream("out.pcm");

        recorder.setRecorderListener(new Recorder.RecorderListener() {
            public void onSoundData(short[] soundData) {
                byte[] outBytes = new byte[soundData.length*2];
                PcmUtil.pcmShortArrayToPcmByteArray(soundData, outBytes);
                try {
                    fileOutputStream.write(outBytes);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        });
    }

}
