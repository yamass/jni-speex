package sound;

import de.yamass.speex.SamplingRate;
import de.yamass.speex.SpeexDecoder;
import de.yamass.speex.SpeexEncoder;

import javax.sound.sampled.AudioFormat;

/**
 * @author Yann Massard
 */
public class MicSpeakerSpeexLoopbackTestWithInstantiableEncoderAndDecoder implements Recorder.RecorderListener {


    private final Recorder recorder;
    private final Player player;
    private final SpeexEncoder speexEncoder;
    private final SpeexDecoder speexDecoder;

    public static void main(String[] args) {
        new MicSpeakerSpeexLoopbackTestWithInstantiableEncoderAndDecoder();
    }

    public MicSpeakerSpeexLoopbackTestWithInstantiableEncoderAndDecoder() {
        speexEncoder = new SpeexEncoder(SamplingRate.ULTRA_WIDEBAND, 27800);
        speexDecoder = new SpeexDecoder(SamplingRate.ULTRA_WIDEBAND);
        recorder = new Recorder(1);
        player = new Player(1, new AudioFormat(32000f, 16, 1, true, false));
        recorder.setRecorderListener(this);
    }

    public void onSoundData(short[] soundData) {

        System.out.println(soundData.length);
        byte[] speex = speexEncoder.encode(soundData);
        System.out.println("Speex bytes: " + speex.length);
        short[] pcm = speexDecoder.decode(speex, speex.length);

        player.addSound(pcm);
    }
}
