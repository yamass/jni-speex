package sound;

/**
 * @author Yann Massard
 */
public class MicSpeakerLoopbackTest {

    public static void main(String[] args) {
        final Recorder recorder = new Recorder(4);
        final Player player = new Player(1);

        recorder.setRecorderListener(new Recorder.RecorderListener() {
            public void onSoundData(short[] soundData) {
                player.addSound(soundData);
            }
        });
    }

}
