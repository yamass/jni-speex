package sound;

import jni.JNI;
import sound.util.PcmUtil;

import java.io.FileInputStream;
import java.io.FileOutputStream;

public class TestEncode {

	public static void main(String[] args) throws Exception {

		int line = 0;
		byte[] pcmBytesIn = new byte[640];
		short[] pcmIn = new short[320];
		short[] pcmOut = new short[320];
		byte[] speex = new byte[200];

		for (int k = 0; k < 10000; k++) {
			line = k % 10;
			FileInputStream in = new FileInputStream("yannIn.wav");
			FileOutputStream out = new FileOutputStream("yannOut.speex");

			JNI.initSpeexEncoder(line, JNI.SPEEX_MODEID_WB);
			JNI.encoderCtl(line, 12, 0); // 12: VBR
			JNI.encoderCtl(line, 4, 5); // 4: Quality
			JNI.encoderCtl(line, 16, 2); // 16: Complexity
			JNI.encoderCtl(line, 30, 1); // 30: VAD

			JNI.initSpeexDecoder(line, JNI.SPEEX_MODEID_WB);

			while (in.read(pcmBytesIn) != -1) {

                PcmUtil.pcmByteArrayToPcmShortArray(pcmBytesIn, pcmIn);

                int numBytes = JNI.encodeSpeex(line, pcmIn, speex);

                out.write(speex, 0, numBytes);

				JNI.decodeSpeex(line, speex, numBytes, pcmOut);

                PcmUtil.pcmShortArrayToPcmByteArray(pcmOut, pcmBytesIn);
                // out.write(pcmBytesIn);
			}

			System.out.println(line);
			JNI.destroySpeexEncoder(line);
			JNI.destroySpeexDecoder(line);

			in.close();
			out.close();

		}
		System.out.println("Finished.");

	}

}
