package sound;

import sound.util.Speakers;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.SourceDataLine;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Player extends Thread {

    private List<short[]> sound;
    private SourceDataLine sourceDataLine;

    public Player(int speakerIndex) {
        this(speakerIndex, new AudioFormat(16000f, 16, 1, true, false));
    }

    public Player(int speakerIndex, AudioFormat audioFormat) {
        try {
            sound = Collections.synchronizedList(new ArrayList<short[]>());
            DataLine.Info lineInfo = new DataLine.Info(SourceDataLine.class, audioFormat, (int) audioFormat.getFrameRate());
            sourceDataLine = Speakers.getSpeaker(speakerIndex);
            sourceDataLine.open(audioFormat);
            sourceDataLine.start();
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addSound(short[] s) {
        if (s == null) {
            System.out.println("DATA IS NULL");
            return;
        }
        sound.add(s);
    }

    public void run() {
        while (true) {
            try {
                if (sound.size() > 0) {
                    short[] s = sound.remove(0);
                    byte[] b = new byte[s.length * 2];
                    for (int j = 0; j < s.length; j++) {
                        b[j * 2] = (byte) (s[j] & 0xff);
                        b[j * 2 + 1] = (byte) (s[j] >>> 8);
                    }
                    sourceDataLine.write(b, 0, b.length);
                } else {
                    sleep(15);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
