package sound;

import jni.JNI;

import java.io.FileInputStream;
import java.io.FileOutputStream;


public class TestEncodePerformance {

	public static void main(String[] args) throws Exception {

		final int LINE = 0;
		short[] pcmIn = new short[320];
		for (int i = 0; i < pcmIn.length; i++) {
			pcmIn[i] = (short) (Math.random()* Short.MAX_VALUE);
		}
		
		short[] pcmOut = new short[320];
		byte[] speex = new byte[200];

		FileInputStream in = new FileInputStream("yannIn.wav");
		FileOutputStream out = new FileOutputStream("yannOut.wav");

		JNI.initSpeexEncoder(LINE, JNI.SPEEX_MODEID_WB);
		JNI.encoderCtl(LINE, 12, 0); //12: VBR
		JNI.encoderCtl(LINE, 4, 5); //4: Quality
		JNI.encoderCtl(LINE, 16, 2); //16: Complexity
		JNI.encoderCtl(LINE, 30, 1); //30: VAD

		JNI.initSpeexDecoder(LINE, JNI.SPEEX_MODEID_WB);

		long startTime = System.currentTimeMillis();
		for (int i=0; i<1000; i++) {
			int numBytes = JNI.encodeSpeex(LINE, pcmIn, speex);
			JNI.decodeSpeex(LINE, speex, numBytes, pcmOut);
		}
		long endTime = System.currentTimeMillis();
		
		System.out.println(endTime-startTime);
		//340
		//320 with j-->n


		JNI.destroySpeexEncoder(LINE);
		JNI.destroySpeexDecoder(LINE);

		in.close();
		out.close();

		System.out.println("Finished.");

	}

}
