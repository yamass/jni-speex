package sound;

import sound.util.Microphones;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.TargetDataLine;

public class Recorder extends Thread {
    private static final int SAMPLE_RATE = 32000;
    private static final int FRAME_SIZE = SAMPLE_RATE / 50;
    private static final int SAMPLE_SIZE_IN_BITS = 16;
    private TargetDataLine targetDataLine;
    private RecorderListener recorderListener;

    public Recorder(int micPortIndex) {
        try {
            AudioFormat audioFormat = new AudioFormat(SAMPLE_RATE, SAMPLE_SIZE_IN_BITS, 1, true, false);
            targetDataLine = Microphones.getMic(audioFormat, micPortIndex);
            targetDataLine.open(audioFormat);
            targetDataLine.start();
            start();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void run() {
        while (true) {
            byte[] b = new byte[FRAME_SIZE * 2];
            targetDataLine.read(b, 0, b.length);
            short[] s = new short[FRAME_SIZE];
            for (int j = 0; j < s.length; j++) {
                s[j] = (short) ((b[2 * j + 1] << 8) | (b[2 * j] & 0xff));
            }

            if (recorderListener != null) {
                recorderListener.onSoundData(s);
            }
        }
    }

    public void setRecorderListener(RecorderListener recorderListener) {
        this.recorderListener = recorderListener;
    }

    public interface RecorderListener {
        public void onSoundData(short[] soundData);
    }
}