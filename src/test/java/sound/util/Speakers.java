package sound.util;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.SourceDataLine;

/**
 * @author Yann Massard
 */
public class Speakers {

    public static SourceDataLine getSpeaker(int pos) {
        try {
            Mixer.Info[] infos = AudioSystem.getMixerInfo();
            AudioFormat audioFormat = new AudioFormat(16000f, 16, 1, true, false);
            DataLine.Info info = new DataLine.Info(SourceDataLine.class, audioFormat);
            int count = 0;
            for (int i = 0; i < infos.length; i++) {
                Mixer mixer = AudioSystem.getMixer(infos[i]);
                if (mixer.isLineSupported(info)) {
                    count++;
                    if (count == pos) {
                        System.out.println(mixer.getMixerInfo().getName());
                        return (SourceDataLine) mixer.getLine(info);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

}
