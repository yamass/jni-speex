package sound.util;

import javax.sound.sampled.AudioFormat;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.DataLine;
import javax.sound.sampled.LineUnavailableException;
import javax.sound.sampled.Mixer;
import javax.sound.sampled.TargetDataLine;
import java.util.ArrayList;
import java.util.List;

/**
 * @author Yann Massard
 */
public class Microphones {
    public static TargetDataLine getMic(int port) throws LineUnavailableException {
        AudioFormat audioFormat = new AudioFormat(16000f, 16, 1, true, false);
        return getMic(audioFormat, port);
    }

    public static TargetDataLine getMic(AudioFormat audioFormat, int port) throws LineUnavailableException {
        Mixer.Info[] mixerInfos = AudioSystem.getMixerInfo();
        DataLine.Info dataLineInfo = new DataLine.Info(TargetDataLine.class, audioFormat);
        int count = 0;
        for (int i = 0; i < mixerInfos.length; i++) {
            Mixer.Info mixerInfo = mixerInfos[i];
            System.out.println(mixerInfo.getName() + " - " + mixerInfo.getDescription());
            Mixer mixer = AudioSystem.getMixer(mixerInfo);
            if (mixer.isLineSupported(dataLineInfo)) {
                count++;
                if (count == port) {
                    System.out.println("SELECTED: " + mixer.getMixerInfo().getDescription());
                    return (TargetDataLine) mixer.getLine(dataLineInfo);
                }
            }
        }
        return null;
    }

    public static void main(String[] args) {
        for (Mixer.Info info : getMixerNamesForAudioFormat(new AudioFormat(44100f, 24, 1, true, false))) {
            System.out.println(info.getName());
        }
    }

    public static List<Mixer.Info> getMixerNamesForAudioFormat(AudioFormat audioFormat) {
        List<Mixer.Info> matchingMixerInfos = new ArrayList<Mixer.Info>();
        Mixer.Info[] mixerInfos = AudioSystem.getMixerInfo();
        DataLine.Info dataLineInfo = new DataLine.Info(TargetDataLine.class, audioFormat);
        for (int i = 0; i < mixerInfos.length; i++) {
            Mixer.Info mixerInfo = mixerInfos[i];
            Mixer mixer = AudioSystem.getMixer(mixerInfo);
            if (mixer.isLineSupported(dataLineInfo)) {
                matchingMixerInfos.add(mixerInfo);
            }
        }
        return matchingMixerInfos;
    }
}
