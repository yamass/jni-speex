package sound.util;

/**
 * @author Yann Massard
 */
public class PcmUtil {

    public static void pcmByteArrayToPcmShortArray(byte[] pcmBytesIn, short[] pcmIn) {
        for (int i = 0; i < pcmIn.length; i++) {
            pcmIn[i] = (short) ((pcmBytesIn[2 * i + 1] << 8) | (pcmBytesIn[2 * i] & 0xff));
        }
    }

    public static void pcmShortArrayToPcmByteArray(short[] pcmShortArray, byte[] pcmByteArray) {
        for (int j = 0; j < pcmShortArray.length; j++) {
            // pcmByteArray[j * 2] = (byte) (pcmShortArray[j] >>> 8);
            // pcmByteArray[j * 2 + 1] = (byte) (pcmShortArray[j]);
            pcmByteArray[j * 2] = (byte) (pcmShortArray[j] & 0xff);
            pcmByteArray[j * 2 + 1] = (byte) (pcmShortArray[j] >>> 8);
        }
    }
}
