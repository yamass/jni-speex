package jni;


import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class JNI {

    public static final int NUMBER_OF_LINES = 1000;
    private static final String[] RESOURCE_NAMES = {
            "libconferendum.dll",
            "libconferendum64.dll",
            "libconferendum.so",
            "libconferendum64.so",
            "libconferendum.dylib",
            "libconferendum32.dylib",
            "libconferendum64.dylib",
            "conferendum.def"
    };

    static {
        try {
            File tempDir = copyResourcesToTempDir();
            for (String resourceName : RESOURCE_NAMES) {
                String libPath = new File(tempDir, resourceName).getAbsolutePath();
                System.out.print("Trying to load " + libPath + " ... ");
                try {
                    System.load(libPath);
                    System.out.println("with SUCCESS.");
                    break;
                } catch (Throwable t) {
                    System.out.println("without success (" + t.getClass() + ").");
                }
            }
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }

    private static File copyResourcesToTempDir() throws IOException {
        File tempDirectory = createTempDirectory();
        System.out.println("copying dynamic libraries to " + tempDirectory.getAbsolutePath());
        for (String resourceName : RESOURCE_NAMES) {
            System.out.println(" - copying " + resourceName);
            byte[] buffer = new byte[1024];
            int read = -1;
            File temp = new File(tempDirectory, resourceName);
            try (InputStream in = JNI.class.getResourceAsStream(resourceName);
                 FileOutputStream fos = new FileOutputStream(temp)) {
                while ((read = in.read(buffer)) != -1) {
                    fos.write(buffer, 0, read);
                }
            }
        }
        return tempDirectory;
    }

    private static File createTempDirectory() throws IOException {
        final File temp;
        temp = File.createTempFile("temp", Long.toString(System.nanoTime()));
        if (!(temp.delete())) {
            throw new IOException("Could not delete temp file: " + temp.getAbsolutePath());
        }
        if (!(temp.mkdir())) {
            throw new IOException("Could not create temp directory: " + temp.getAbsolutePath());
        }
        return (temp);
    }

    /**
     * modeID for the defined narrowband mode
     */
    public static final int SPEEX_MODEID_NB = 0;
    /**
     * modeID for the defined wideband mode
     */
    public static final int SPEEX_MODEID_WB = 1;
    /**
     * modeID for the defined ultra-wideband mode
     */
    public static final int SPEEX_MODEID_UWB = 2;

    public static native void initSpeexEncoder(int line, int modeId);

    public static native int encoderCtl(int line, int option, int value);

    public static native void preprocessStateInit(int line, int frameSize, int samplingRate);

    public static native int preprocessCtl(int line, int option, int value);

    public static native void preprocessRun(int line, short[] audio);

    public static native void echoStateInit(int line, int frameSize, int filterLength);

    public static native void bindEchoCancellationToPreprocessor(int line);

    public static native int speex_echo_ctl(int line, int option, int value);

    public static native void speex_echo_cancellation(int line, short[] input, short[] echo, short[] out);

    public static native void speex_echo_playback(int line, short[] frame);

    public static native void speex_echo_capture(int line, short[] inputFrame, short[] outputFrame);

    public static native void speex_echo_state_destroy(int line);

    public static native void speex_echo_state_reset(int line);

    public static native void initSpeexDecoder(int line, int modeId);

    public static native int speex_decoder_ctl(int line, int option, int value);

    public static native int encodeSpeex(int line, short[] pcm, byte[] speex);

    public static native void decodeSpeex(int line, byte[] speex, int len, short[] pcm);

    public static native void destroySpeexEncoder(int line);

    public static native void destroySpeexDecoder(int line);

    private static native byte[][] listWindowsWindowsNative();

    public static String[] listWindowsWindows() {
        byte[][] names = JNI.listWindowsWindowsNative();
        int len = 0;
        for (int i = 0; i < names.length; i++) {
            if (names[i] == null) {
                break;
            }
            len++;
        }
        String[] result = new String[len];
        for (int i = 0; i < len; i++) {
            result[i] = new String(names[i]);
        }
        return result;
    }
}
