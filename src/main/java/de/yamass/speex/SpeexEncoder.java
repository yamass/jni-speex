package de.yamass.speex;

import jni.JNI;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Yann Massard
 */
public class SpeexEncoder {

    private static final Map<Integer, SpeexEncoder> lineToSpeexEncoder = Collections.synchronizedMap(new HashMap<Integer, SpeexEncoder>());
    private final int lineNumber;
    private final SamplingRate samplingRate;
    private volatile boolean closed;

    public SpeexEncoder(SamplingRate samplingRate, int bitrate) {
        this.samplingRate = samplingRate;
        lineNumber = reserveLine();
        JNI.initSpeexEncoder(lineNumber, samplingRate.getConstant());
        JNI.encoderCtl(lineNumber, 12, 0); //12: VBR
//        JNI.encoderCtl(lineNumber, 4, 5); //4: Quality
        JNI.encoderCtl(lineNumber, 18, bitrate); //18: Bitrate
        JNI.encoderCtl(lineNumber, 16, 4); //16: Complexity
        JNI.encoderCtl(lineNumber, 30, 1); //30: VAD
    }

    private int reserveLine() {
        int lineNumber;
        synchronized (lineToSpeexEncoder) {
            boolean sucessfullyAssigned = false;
            for (lineNumber = 0; lineNumber < JNI.NUMBER_OF_LINES; lineNumber++) {
                if (lineToSpeexEncoder.get(lineNumber) == null) {
                    lineToSpeexEncoder.put(lineNumber, this);
                    sucessfullyAssigned = true;
                    break;
                }
            }
            if (!sucessfullyAssigned) {
                throw new IllegalStateException("No more speex line available!");
            }
        }
        return lineNumber;
    }

    public byte[] encode(short[] soundData) {
        if (closed) {
            throw new IllegalStateException("Encoder already closed!");
        }
        byte[] speexBuffer = new byte[(samplingRate.getSamplingRateHz() / 50) * 2]; // 20 ms
        int len = JNI.encodeSpeex(lineNumber, soundData, speexBuffer);
        byte[] speexBytes = new byte[len];
        System.arraycopy(speexBuffer, 0, speexBytes, 0, len);
        return speexBytes;
    }

    public void close() {
        closed = true;
        lineToSpeexEncoder.remove(lineNumber);
        JNI.destroySpeexEncoder(lineNumber);
    }

}
