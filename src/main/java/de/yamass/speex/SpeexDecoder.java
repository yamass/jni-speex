package de.yamass.speex;

import jni.JNI;

import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

/**
 * @author Yann Massard
 */
public class SpeexDecoder {

    private static final Map<Integer, SpeexDecoder> lineToSpeexDecoder = Collections.synchronizedMap(new HashMap<Integer, SpeexDecoder>());
    private final int lineNumber;
    private final SamplingRate samplingRate;
    private volatile boolean closed;

    public SpeexDecoder(SamplingRate samplingRate) {
        this.samplingRate = samplingRate;
        lineNumber = reserveLine();
        JNI.initSpeexDecoder(lineNumber, samplingRate.getConstant());
    }

    private int reserveLine() {
        int lineNumber;
        synchronized (lineToSpeexDecoder) {
            boolean sucessfullyAssigned = false;
            for (lineNumber = 0; lineNumber < JNI.NUMBER_OF_LINES; lineNumber++) {
                if (lineToSpeexDecoder.get(lineNumber) == null) {
                    lineToSpeexDecoder.put(lineNumber, this);
                    sucessfullyAssigned = true;
                    break;
                }
            }
            if (!sucessfullyAssigned) {
                throw new IllegalStateException("No more speex line available!");
            }
        }
        return lineNumber;
    }

    public short[] decode(byte[] speexData, int speexDataLength) {
        if (closed) {
            throw new IllegalStateException("Decoder already closed!");
        }
        short[] pcmBuffer = new short[(samplingRate.getSamplingRateHz() / 50)]; // 20 ms
        JNI.decodeSpeex(lineNumber, speexData, speexDataLength, pcmBuffer);
        return pcmBuffer;
    }

    public void close() {
        closed = true;
        lineToSpeexDecoder.remove(lineNumber);
        JNI.destroySpeexDecoder(lineNumber);
    }



}
