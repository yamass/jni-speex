package de.yamass.speex;

import jni.JNI;

/**
 * @author Yann Massard
 */
public enum SamplingRate {

    NARROWBAND(JNI.SPEEX_MODEID_NB, 8000),
    WIDEBAND(JNI.SPEEX_MODEID_WB, 16000),
    ULTRA_WIDEBAND(JNI.SPEEX_MODEID_UWB, 32000);

    private final int constant;
    private final int samplingRateHz;

    SamplingRate(int constant, int samplingRateHz) {

        this.constant = constant;
        this.samplingRateHz = samplingRateHz;
    }

    public int getConstant() {
        return constant;
    }

    public int getSamplingRateHz() {
        return samplingRateHz;
    }
}
